#include <FlexCAN.h>
#include "settings.h"
#include "parameters.h"
//----------------------CAN BUS FRAMES
//0x19B50001 - OVERALL
//0x19B50002 - BATTERY
//0x19B50003 - ENGINE
//0x19B50004 - LIGHTS
//0x19B50005 - LATITUDE
//0x19B50006 - LONGITUDE
//0x19B50007 - ACTIONS
static CAN_message_t msg;

byte START_MESSAGE[2] = {0x1E, 0x1F};
byte START_COORDINATES_MESSAGE = 0x69;
//----------------------
elapsedMillis timerSendData;
elapsedMillis timerBluetoothSendData;

void setup(void)
{
  Serial.begin(9600);
  BTSerial.begin(9600);
  delay(500);
  Serial.println("Teensy 3.2 Transmitter started.");
  pinMode(BLUETOOTH_PIN_STATE, INPUT_PULLUP);
  pinMode(NAVIGATION_STATUS_PIN, INPUT_PULLUP);
  pinMode(DASHBOARD_STATUS_PIN, INPUT_PULLUP);
  pinMode(DRIVING_MODE_PIN, INPUT_PULLUP);
  pinMode(LEFT_LIGHT, INPUT_PULLUP);
  pinMode(RIGHT_LIGHT, INPUT_PULLUP);
  pinMode(BOTH_LIGHTS, INPUT_PULLUP);
  pinMode(SHORT_LIGHT, INPUT_PULLUP);
  pinMode(PARKING_LIGHTS, INPUT_PULLUP);
  pinMode(EMERGENCY_LIGHTS, INPUT_PULLUP);
  pinMode(LONG_LIGHT, INPUT_PULLUP);

   pinMode(IS_CHARGING_PIN, INPUT_PULLDOWN);

/*
  pinMode(PIN_1,INPUT_PULLDOWN);
  pinMode(PIN_2,INPUT_PULLDOWN);
  pinMode(PIN_3,INPUT_PULLDOWN);
  pinMode(PIN_4,INPUT_PULLDOWN);
  pinMode(PIN_5,INPUT_PULLDOWN);
  pinMode(PIN_6,INPUT_PULLDOWN);
  */
  
  attachInterrupt(digitalPinToInterrupt(NAVIGATION_STATUS_PIN), navigationButton, FALLING);
  attachInterrupt(digitalPinToInterrupt(DASHBOARD_STATUS_PIN), dashboardButton, FALLING);
  attachInterrupt(digitalPinToInterrupt(DRIVING_MODE_PIN), drvingModeButton, FALLING);
  attachInterrupt(digitalPinToInterrupt(IS_CHARGING_PIN), isChargingButton, RISING);
  attachInterrupt(digitalPinToInterrupt(BLUETOOTH_PIN_STATE), connectionState, CHANGE);
  attachInterrupt(digitalPinToInterrupt(LEFT_LIGHT), leftLight, FALLING);
  attachInterrupt(digitalPinToInterrupt(RIGHT_LIGHT), rightLight, FALLING);
  attachInterrupt(digitalPinToInterrupt(BOTH_LIGHTS), bothLights, FALLING);
  attachInterrupt(digitalPinToInterrupt(SHORT_LIGHT), shortLight, FALLING);
  attachInterrupt(digitalPinToInterrupt(PARKING_LIGHTS), parkingLights, FALLING);
  attachInterrupt(digitalPinToInterrupt(EMERGENCY_LIGHTS), emergencyLights, FALLING);
  attachInterrupt(digitalPinToInterrupt(LONG_LIGHT), longLight, FALLING);

  Can0.begin(250000);

  msg.ext = 1;
  msg.id = 0x19B50001;
  msg.len = 8;
  msg.buf[0] = 0x0;
  msg.buf[1] = 0x0;
  msg.buf[2] = 0x0;
  msg.buf[3] = 0x0;
  msg.buf[4] = 0x0;
  msg.buf[5] = 0x00;
  msg.buf[6] = 0x00;
  msg.buf[7] = 0x00;

  timerSendData = 0;
  timerBluetoothSendData = 0;
  connectionState();
}


// -------------------------------------------------------------
void loop(void)
{
  if (timerSendData > TIMESTAMP)
  {
    timerSendData = 0;
    //OVERALL
    engine_current = map(analogRead(PIN_1), 0, 1023, 0, 50);
    speed_value = map(analogRead(PIN_2), 0, 1023, 0, 160);
    battery_soc = map(analogRead(PIN_3), 0, 1023, 0, 100);
    battery_temperature = map(analogRead(PIN_4), 0, 1023, 0, 70);
    engine_temperature = map(analogRead(PIN_5), 0, 1023, 0, 100);
    battery_left_distance = map(analogRead(PIN_6), 0, 1023, 0, 400);

    msg.id = 0x19B50001;
    msg.buf[0] = speed_value;
    msg.buf[1] = bluetooth_status;
    msg.buf[2] = dashboard_status;
    msg.buf[3] = navigation_status;
    msg.buf[4] = 0;
    msg.buf[5] = 0;
    msg.buf[6] = 0;
    msg.buf[7] = 0;
    Can0.write(msg);

    //BATTERY
    msg.id = 0x19B50002;
    msg.buf[0] = battery_soc;
    msg.buf[1] = battery_is_charging;
    msg.buf[2] = battery_temperature;
    uint8_t LSB_battery_left_distance = battery_left_distance;
    uint8_t MSB_battery_left_distance = battery_left_distance >> 8;
    msg.buf[3] = LSB_battery_left_distance;
    msg.buf[4] = MSB_battery_left_distance;
    msg.buf[5] = 0;
    msg.buf[6] = 0;
    msg.buf[7] = 0;
    Can0.write(msg);

    //ENGINE
    msg.id = 0x19B50003;
    if(driving_mode == 4)
    {
      msg.buf[0] = 3;
    }
    else
    {
      msg.buf[0] = driving_mode;
    }
    msg.buf[1] = engine_current;
    msg.buf[2] = engine_voltage;
    msg.buf[3] = engine_temperature;
    msg.buf[4] = 0;
    msg.buf[5] = 0;
    msg.buf[6] = 0;
    msg.buf[7] = 0;
    Can0.write(msg);

    //LIGHTS
    msg.id = 0x19B50004;
    msg.buf[0] = short_lights;
    msg.buf[1] = long_lights;
    msg.buf[2] = parking_lights;
    msg.buf[3] = direction_lights;
    msg.buf[4] = emergency_lights;
    msg.buf[5] = 0;
    msg.buf[6] = 0;
    msg.buf[7] = 0;
    Can0.write(msg);
  }


  while (BTSerial.available()) 
  {
    byte in_msg[MSG_LENGHT];
    in_msg[0] = BTSerial.read();
    delay(2);
    in_msg[1] = BTSerial.read();
    delay(2);
    in_msg[2] = BTSerial.read();
    delay(2);
    in_msg[3] = BTSerial.read();
    delay(2);
    in_msg[4] = BTSerial.read();
    delay(2);
    in_msg[5] = BTSerial.read();
    delay(2);
    in_msg[6] = BTSerial.read();
    delay(2);
    in_msg[7] = BTSerial.read();

    if (in_msg[0] == START_COORDINATES_MESSAGE)
    {
      switch (in_msg[1])
      {
        case LATTITUDE_ID:
          {
            uint8_t LSB = in_msg[2];
            uint8_t SECOND_BYTE = in_msg[3];
            uint8_t THIRD_BYTE = in_msg[4];
            uint8_t FOURTH_BYTE = in_msg[5];
            uint8_t MSB = in_msg[6];
            int decimalVal = (int)in_msg[7];

            //SEND LATITUDE VIA CAN BUS
            msg.id = 0x19B50005;
            msg.buf[0] = in_msg[2];
            msg.buf[1] = in_msg[3];
            msg.buf[2] = in_msg[4];
            msg.buf[3] = in_msg[5];
            msg.buf[4] = in_msg[6];
            msg.buf[5] = in_msg[7];
            msg.buf[6] = 0;
            msg.buf[7] = 0;
            Can0.write(msg);

            long newVal = (MSB << 32) | (FOURTH_BYTE << 24) | (THIRD_BYTE << 16) | (SECOND_BYTE << 8) | LSB;

            if (decimalVal == 1)
            {
              lat_destination = (double)(newVal / (pow(10, 8)));
            }
            else if (decimalVal == 2)
            {
              lat_destination = (double)(newVal / (pow(10, 7)));
            }
            else if (decimalVal == 3)
            {
              lat_destination = (double)(newVal / (pow(10, 6)));
            }

            if(DEBUG)
            {
            Serial.println("");
            Serial.println("------------------------------");
            Serial.print("Decoded LAT Value "); Serial.println(lat_destination, 7);
            }

            break;
          }

        case LONGITUDE_ID:
          {
            uint8_t LSB = in_msg[2];
            uint8_t SECOND_BYTE = in_msg[3];
            uint8_t THIRD_BYTE = in_msg[4];
            uint8_t FOURTH_BYTE = in_msg[5];
            uint8_t MSB = in_msg[6];
            int decimalVal = (int)in_msg[7];

            //SEND LONGITUDE VIA CAN BUS
            msg.id = 0x19B50006;
            msg.buf[0] = in_msg[2];
            msg.buf[1] = in_msg[3];
            msg.buf[2] = in_msg[4];
            msg.buf[3] = in_msg[5];
            msg.buf[4] = in_msg[6];
            msg.buf[5] = in_msg[7];
            msg.buf[6] = 0;
            msg.buf[7] = 0;
            Can0.write(msg);

            long newVal = (MSB << 32) | (FOURTH_BYTE << 24) | (THIRD_BYTE << 16) | (SECOND_BYTE << 8) | LSB;

            if (decimalVal == 1)
            {
              lon_destination = (double)(newVal / (pow(10, 8)));
            }
            else if (decimalVal == 2)
            {
              lon_destination = (double)(newVal / (pow(10, 7)));
            }
            else if (decimalVal == 3)
            {
              lon_destination = (double)(newVal / (pow(10, 6)));
            }

            if(DEBUG)
            {
            Serial.print("Decoded LON Value "); Serial.println(lon_destination, 7);
            Serial.println("------------------------------");
            }
            break;
          }

        default:
          if (DEBUG)
          {
            Serial.println("[!] Unknown message");
          }
          break;
      }
    }

    if ((in_msg[0] == START_MESSAGE[0]) && (in_msg[1] == START_MESSAGE[1]))
    {
      switch (in_msg[2])
      {

        case PERFORMANCE_ID:
          {
            driving_mode = (int)in_msg[3];
            max_speed = (int)in_msg[4];
            max_torque = (int)in_msg[5];
            brake_regeneration = (int)in_msg[6];

            if (DEBUG)
            {
              Serial.println("");
              Serial.println("------------------------------");
              Serial.print("Driving mode: "); Serial.println(driving_mode);
              Serial.print("Max_speed "); Serial.println(max_speed);
              Serial.print("Max_torque: "); Serial.println(max_torque);
              Serial.print("Brake_regeneration: "); Serial.println(brake_regeneration);
              Serial.println("------------------------------");
            }
            break;
          }

        case SETTINGS_ID:
          {
            auto_lights = (bool)in_msg[3];
            back_home_lights = (bool)in_msg[4];
            battery_soc_lights = (bool)in_msg[5];
            start_buzzer_sequence = (bool)in_msg[6];
            voice_controll = (bool)in_msg[7];

            if (DEBUG)
            {
              Serial.println("");
              Serial.println("------------------------------");
              Serial.print("auto_lights: "); Serial.println(auto_lights);
              Serial.print("back_home_lights: "); Serial.println(back_home_lights);
              Serial.print("battery_soc_lights: "); Serial.println(battery_soc_lights);
              Serial.print("start_buzzer_sequence: "); Serial.println(start_buzzer_sequence);
              Serial.print("voice_controll: "); Serial.println(voice_controll);
              Serial.println("------------------------------");
            }
            break;
          }

        case ACTIONS_ID:
          {

            bool temp_nav = (bool)in_msg[3];
            if (temp_nav == true)
            {
              if (dashboard_status == false)
              {
                if (navigation_status == false)
                {
                  dashboard_status = true;
                  navigation_status = true;
                }
              }
            }

            if (DEBUG)
            {
              Serial.println("");
              Serial.println("------------------------------");
              Serial.print("start_navigation: "); Serial.println(navigation_status);
              Serial.print("acction2 slot: "); Serial.println("FREE");
              Serial.print("acction3 slot: "); Serial.println("FREE");
              Serial.print("acction4 slot: "); Serial.println("FREE");
              Serial.print("acction5 slot: "); Serial.println("FREE");
              Serial.println("------------------------------");
            }

            break;
          }

        default:
          if (DEBUG)
          {
            Serial.println("Unknown message");
          }
          break;

      }
    }
    //clear buffer
    for (int i = 0; i < MSG_LENGHT; i++)
    {
      in_msg[i] = 0;
    }
  }

  //send bluetooth data
  if (digitalRead(BLUETOOTH_PIN_STATE) == HIGH)
  {
    if (timerBluetoothSendData == SEND_INTERVAL)
    {
      sendBatteryEnergy();
    }
    if (timerBluetoothSendData == 2 * SEND_INTERVAL)
    {
      sendBatteryChargingParameters();
    }
    if (timerBluetoothSendData == 3 * SEND_INTERVAL)
    {
      sendBatteryOverallParameters();
    }
    if (timerBluetoothSendData == 4 * SEND_INTERVAL)
    {
      sendMotoOverallParameters();
    }
    if (timerBluetoothSendData == 5 * SEND_INTERVAL)
    {
      sendMotoStatsOveral();
    }
    if (timerBluetoothSendData == 6 * SEND_INTERVAL)
    {
      sendPerformanceSettings();
    }
    if (timerBluetoothSendData == 7 * SEND_INTERVAL)
    {
      sendSettings();
    }
    if (timerBluetoothSendData > 7 * SEND_INTERVAL)
    {
      timerBluetoothSendData = 0;
    }
  }
}

//<---------------------------------------FUNCTIONS
void navigationButton()
{
  delay(60);
  if (digitalRead(NAVIGATION_STATUS_PIN) == 0)
  {
    //navigation_status = !navigation_status;
    if (dashboard_status == false)
    {
      delay(60);
      if (navigation_status == false)
      {
        dashboard_status = true;
        navigation_status = true;
      }
    }
    else
    {
      delay(60);
      navigation_status = !navigation_status;
    }
    Serial.print("Navigation status: "); Serial.println(navigation_status);
  }
}
void dashboardButton()
{
  delay(60);
  if (digitalRead(DASHBOARD_STATUS_PIN) == 0)
  {
    delay(60);
    dashboard_status = !dashboard_status;
    Serial.print("Dashboard status: "); Serial.println(dashboard_status);
  }
}

void drvingModeButton()
{
  delay(60);
  if(digitalRead(DRIVING_MODE_PIN) == LOW)
  {
    delay(60);
    driving_mode = driving_mode + 1;
    if (driving_mode > 4)
    {
      driving_mode = 1;
    }
  }
  Serial.print("Driving Mode: "); Serial.println(driving_mode);
}

void isChargingButton()
{
  delay(60);
  if(digitalRead(IS_CHARGING_PIN) == HIGH)
  {
    if(battery_is_charging == false)
    {
      delay(60);
      battery_is_charging = true;
    }
    else
    {
      delay(60);
      battery_is_charging = false;
    }
    Serial.print("Battery isCharging: "); Serial.println(battery_is_charging);
  }
}

void leftLight()
{
  delay(60);
  if(digitalRead(LEFT_LIGHT) == LOW)
  {
    if(direction_lights == 1)
    {
      delay(60);
      direction_lights = 0;
    }
    else
    {
      delay(60);
      direction_lights = 1;
    }
    Serial.print("Direction lights: "); Serial.println(direction_lights);
  }
}

void rightLight()
{
  delay(60);
  if(digitalRead(RIGHT_LIGHT) == LOW)
  {
    if(direction_lights == 2)
    {
      delay(60);
      direction_lights = 0;
    }
    else
    {
      delay(60);
      direction_lights = 2;
    }
    Serial.print("Direction lights: "); Serial.println(direction_lights);
  }
}

void bothLights()
{
  delay(60);
  if(digitalRead(BOTH_LIGHTS) == LOW)
  {
    if(direction_lights == 3)
    {
      delay(60);
      direction_lights = 0;
    }
    else
    {
      delay(60);
      direction_lights = 3;
    }
    Serial.print("Direction lights: "); Serial.println(direction_lights);
  }
}

void shortLight()
{
  delay(60);
  if(digitalRead(SHORT_LIGHT) == LOW)
  {
    if(short_lights == false)
    {
      delay(60);
      short_lights = true;
    }
    else
    {
      delay(60);
      short_lights = false;
    }
    Serial.print("Short lights: "); Serial.println(short_lights);
  }
}

void parkingLights()
{
  delay(60);
  if(digitalRead(PARKING_LIGHTS) == LOW)
  {
    if(parking_lights == false)
    {
      delay(60);
      parking_lights = true;
    }
    else
    {
      delay(60);
      parking_lights = false;
    }
    Serial.print("Parking lights: "); Serial.println(parking_lights);
  }
}

void emergencyLights()
{
  delay(60);
  if(digitalRead(EMERGENCY_LIGHTS) == LOW)
  {
    if(emergency_lights == false)
    {
      delay(60);
      emergency_lights = true;
    }
    else
    {
      delay(60);
      emergency_lights = false;
    }
    Serial.print("Emergency lights: "); Serial.println(emergency_lights);
  }
}

void longLight()
{
  delay(60);
  if(digitalRead(LONG_LIGHT) == LOW)
  {
    if(long_lights == false)
    {
      delay(60);
      long_lights = true;
    }
    else
    {
      delay(60);
      long_lights = false;
    }
    Serial.print("Long lights: "); Serial.println(long_lights);
  }
}
//<---------------------------------------

void connectionState()
{
  if (digitalRead(BLUETOOTH_PIN_STATE) == HIGH)
  {
    Serial.println("[*] BLUETOOTH CONNECTED");
    bluetooth_status = true;
  }
  else
  {
    Serial.println("[*] BLUETOOTH DISCONNECTED");
    bluetooth_status = false;
  }
}

void sendBatteryEnergy()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = BATTERY_ENERGY_ID;
  msg[3] = battery_soc;
  msg[4] = battery_voltage;
  msg[5] = battery_cell_balance;
  msg[6] = battery_temperature;
  msg[7] = 0;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendBatteryOverallParameters()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = BATTERY_OVERALL_ID;
  uint8_t LSB = battery_left_distance;
  uint8_t MSB = battery_left_distance >> 8;
  msg[3] = LSB;
  msg[4] = MSB;
  LSB = battery_charge_cycles;
  MSB = battery_charge_cycles >> 8;
  msg[5] = LSB;
  msg[6] = MSB;
  msg[7] = 0;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendBatteryChargingParameters()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = BATTERY_CHARGING_ID;
  msg[3] = battery_is_charging;
  msg[4] = battery_charging_amps;
  msg[5] = battery_time_to_full_charge;
  msg[6] = 0;
  msg[7] = 0;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendMotoOverallParameters()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = MOTO_OVERALL_ID;
  uint8_t LSB = milleage;
  uint8_t MSB = milleage >> 8;
  msg[3] = LSB;
  msg[4] = MSB;
  LSB = odometer;
  MSB = odometer >> 8;
  msg[5] = LSB;
  msg[6] = MSB;
  msg[7] = engine_temperature;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendMotoStatsOveral()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = STATS_OVERALL_ID;
  msg[3] = max_registered_speed;
  msg[4] = max_registered_angle;
  msg[5] = max_registered_torque;
  msg[6] = 0;
  msg[7] = 0;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendPerformanceSettings()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = PERFORMANCE_ID;
  msg[3] = driving_mode;
  msg[4] = max_speed;
  msg[5] = max_torque;
  msg[6] = brake_regeneration;
  msg[7] = 0;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}

void sendSettings()
{
  BTSerial.flush();
  byte msg[MSG_LENGHT];
  msg[0] = START_MESSAGE[0];
  msg[1] = START_MESSAGE[1];
  msg[2] = SETTINGS_ID;
  msg[3] = auto_lights;
  msg[4] = back_home_lights;
  msg[5] = battery_soc_lights;
  msg[6] = start_buzzer_sequence;
  msg[7] = voice_controll;
  BTSerial.write(msg, MSG_LENGHT);
  BTSerial.flush();
}
