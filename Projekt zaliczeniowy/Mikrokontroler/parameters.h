#include <arduino.h>

//<-----PARAMAETERS
//battery
volatile int battery_soc = 0; //--------CAN
volatile int battery_voltage = 0; //--------CAN
volatile int battery_cell_balance = 0;
volatile int battery_temperature = 0;
volatile int battery_left_distance = 0; //--------CAN
volatile int battery_charge_cycles = 0;
volatile int battery_time_to_full_charge = 0;
volatile int battery_charging_amps = 0;
volatile bool battery_is_charging = 0; //--------?CAN?

//drivetrain
volatile int milleage = 0;
volatile int odometer = 0; //--------CAN
volatile int engine_temperature = 0; //--------CAN
volatile int max_registered_speed = 0;
volatile int max_registered_angle = 0;
volatile int max_registered_torque = 0;

//performance
volatile int driving_mode = 1; //--------CAN
volatile int max_speed = 0;
volatile int max_torque = 0;
volatile int brake_regeneration = 0;

//settings
volatile bool auto_lights = 0;
volatile bool back_home_lights = 0;
volatile bool battery_soc_lights = 0;
volatile bool start_buzzer_sequence = 0;
volatile bool voice_controll = 0;
//GPS
volatile double lat_destination = 0; //--------CAN
volatile double lon_destination = 0; //--------CAN
//OVERALL (FROM DASHBOARD)
volatile int speed_value = 0; //--------CAN
volatile int engine_current = 0;
volatile int engine_voltage = 1;
volatile bool bluetooth_status = false; //--------CAN
volatile bool dashboard_status = false; //--------CAN
volatile bool navigation_status = false; //--------CAN
//LIGHTS
volatile bool short_lights = false;
volatile bool long_lights = false;
volatile bool parking_lights = false;
volatile int direction_lights = 0;
volatile bool emergency_lights = false;

//<---------------------------------------
