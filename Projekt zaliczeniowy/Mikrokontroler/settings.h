#include <arduino.h>

//---------------------- BUTTONS AND OTHERS
#define PIN_1 A0
#define PIN_2 A1
#define PIN_3 A2
#define PIN_4 A3
#define PIN_5 A4
#define PIN_6 A5

#define NAVIGATION_STATUS_PIN 6
#define DASHBOARD_STATUS_PIN 5
#define DRIVING_MODE_PIN 2
#define IS_CHARGING_PIN 13
//lights
#define LEFT_LIGHT 8
#define RIGHT_LIGHT 7
#define BOTH_LIGHTS 11
#define PARKING_LIGHTS 12
#define SHORT_LIGHT 23
#define EMERGENCY_LIGHTS 21
#define LONG_LIGHT 20
#define TIMESTAMP 20
//----------------------Bluetooth
#define BLUETOOTH_PIN_STATE 22
#define BTSerial Serial2
#define SEND_INTERVAL 320
#define DEBUG true

#define MSG_LENGHT           8
#define BATTERY_ENERGY_ID    0x1 //[3]soc, [4]voltage, [5]cell_balance, [6]temp,          
#define BATTERY_OVERALL_ID   0x2 //[3,4]left_distance, [5,6] charge_cycles
#define BATTERY_CHARGING_ID  0x3 //[3]isCharging, [4]charging_Amps, [5]time_to_full_charge
#define MOTO_OVERALL_ID      0x5 //[3,4]milleage, [5,6]odometer, [7]engine_temp
#define STATS_OVERALL_ID     0x6 //[3]max_registered_speed, [4]max_angle, [5]max_torque
#define PERFORMANCE_ID       0x4 //[3]driving_mode, [4]max_speed, [5]max_torque, [6]brake_regeneration
#define SETTINGS_ID          0x7 //[3]auto_lights, [4]back_home_lights, [5]battery_soc_lights, [6]start_buzzer_sequence, [7]voice_controll
#define LATTITUDE_ID         0x8
#define LONGITUDE_ID         0x9
#define ACTIONS_ID           0xA
