package com.mobile.smartconnectdashboard;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {
    ActionBar actionBar;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = (BottomNavigationView)findViewById(R.id.mainNavigationMenu);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.homeActivityTitle);
        MotoParameters.setupDefaultParamateres();
        createBottomMenu();
    }
    private void createBottomMenu()
    {
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        //bottom navigation view activities
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId())
                {

                    case R.id.home_menu:
                        selectedFragment = HomeFragment.newInstance();
                        break;

                    case R.id.battery_menu:
                        selectedFragment = BatteryFragment.newInstance();
                        break;

                    case R.id.performance_menu:
                        selectedFragment = PerformanceFragment.newInstance();
                        break;

                    case R.id.drivetrain_menu:
                        selectedFragment = DrivetrainFragment.newInstance();
                        break;

                    case R.id.settings_menu:
                        selectedFragment = SettingsFragment.newInstance();
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
                return true;
            }
        });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
        transaction.commit();
    }
    public void onBackPressed() {

        LoginActivity.bt_ConnectedThread.cancel();
        finishAffinity();
        System.exit(0);
    }
}
