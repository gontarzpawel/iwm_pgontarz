package com.mobile.smartconnectdashboard;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeFragment extends Fragment {

    TextView batterySOCHomeTextView;
    TextView isChargingTextView;
    ImageView batteryImageView;
    Handler handler = new Handler();
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            updateParameters();
            handler.postDelayed(this, 1000);
        }
    };

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        batterySOCHomeTextView = (TextView)view.findViewById(R.id.batterySOCHomeTextView);
        isChargingTextView = (TextView)view.findViewById(R.id.chargingStatusTextView);
        batteryImageView = (ImageView)view.findViewById(R.id.batteryImageView);

        updateParameters();
        handler.post(runnableCode);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(runnableCode);
        handler.removeCallbacks(runnableCode);
    }

    private void updateParameters()
    {
        batterySOCHomeTextView.setText(String.valueOf(MotoParameters.getBattery_state_of_charge())+"%");
        if(MotoParameters.isCharging())
        {
            isChargingTextView.setText("Charging");
        }
        else
        {
            isChargingTextView.setText("Discharging");
        }
        //change battery icon
        if(MotoParameters.isCharging())
        {
            if(MotoParameters.getBattery_state_of_charge() < 30)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_20_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 30 & MotoParameters.getBattery_state_of_charge() < 50)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_40_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 50 & MotoParameters.getBattery_state_of_charge() < 60)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_50_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 60 & MotoParameters.getBattery_state_of_charge() < 80)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_60_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 80 & MotoParameters.getBattery_state_of_charge() < 90)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_80_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 90 & MotoParameters.getBattery_state_of_charge() < 100)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_90_charging);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 100)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_100_charging);
            }
        }
        else
        {
            if(MotoParameters.getBattery_state_of_charge() < 10)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_0);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 10 & MotoParameters.getBattery_state_of_charge() < 30)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_20);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 30 & MotoParameters.getBattery_state_of_charge() < 50)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_40);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 50 & MotoParameters.getBattery_state_of_charge() < 60)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_50);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 60 & MotoParameters.getBattery_state_of_charge() < 80)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_60);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 80 & MotoParameters.getBattery_state_of_charge() < 90)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_80);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 90 & MotoParameters.getBattery_state_of_charge() < 100)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_90);
            }
            if(MotoParameters.getBattery_state_of_charge() >= 100)
            {
                batteryImageView.setImageResource(R.drawable.ic_battery_100);
            }
        }
    }
}