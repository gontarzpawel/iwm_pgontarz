package com.mobile.smartconnectdashboard;
import android.util.Log;

public class MotoParameters {
    //<-------------Messages ID's
    public static final byte MSG_LENGHT = 8;
    public static final byte MSG_START[] = {0x1E,0x1F};
    public static final byte START_COORDINATES_MESSAGE = 0x69;

    public static final byte BATTERY_ENERGY_ID  = 0x1; //[3]soc, [4]voltage, [5]cell_balance, [6]temp,
    public static final byte BATTERY_OVERALL_ID = 0x2; //[3,4]left_distance, [5,6] charge_cycles
    public static final byte BATTERY_CHARGING_ID= 0x3; //[3]isCharging, [4]charging_Amps, [5]time_to_full_charge
    public static final byte PERFORMANCE_ID =     0x4; //[3]driving_mode, [4]max_speed, [5]max_torque, [6]brake_regeneration
    public static final byte MOTO_OVERALL_ID =    0x5; //[3,4]milleage, [5,6]odometer, [7]engine_temp
    public static final byte STATS_OVERALL_ID =   0x6; //[3]max_registered_speed, [4]max_angle, [5]max_torque
    public static final byte SETTINGS_ID =        0x7; //[3]auto_lights, [4]back_home_lights, [5]battery_soc_lights, [6]start_buzzer_sequence, [7]voice_controll
    public static final byte LATTITUDE_ID =       0x8; //[2,3,4,5,6]lat_data(long), [7]decimal_value
    public static final byte LONGITUDE_ID =       0x9; //[2,3,4,5,6]long_data(long), [7]decimal_value
    public static final byte ACTIONS_ID =         0xA; //[3]start_navigation

    //<-------------Battery
    private static int battery_left_distance;
    private static int battery_state_of_charge;
    private static int battery_voltage;
    private static int battery_cell_balance;
    private static int battery_temperature;
    private static int battery_charge_cycles;
    private static int battery_time_to_full_charge;
    private static int battery_charging_amps;
    private static boolean charging; // 0 -> OFF, 1 -> ON

    //<-------------Performance
    private static int driving_mode; // 1 -> ECO, 2 -> NORMAL, 3->SPORT, 4-> CUSTOM
    public static final int max_speed_eco = 45;
    public static final int max_speed_normal = 35;
    public static final int max_speed_sport = 80;
    public static final int max_torque_eco = 45;
    public static final int max_torque_normal = 65;
    public static final int max_torque_sport = 80;
    public static final int brake_regeneration_eco = 80;
    public static final int brake_regeneration_normal = 65;
    public static final int brake_regeneration_sport = 30;
    private static int custom_max_speed;
    private static int custom_max_torque;
    private static int custom_brake_regeneration;

    //<-------------Drivetrain
    private static int milleage;
    private static int registered_max_speed;
    private static int odometer;
    private static int engine_temperature;
    private static int registered_max_angle;
    private static int registered_max_torque;

    //<-------------Settings
    private static int auto_lights;
    private static int back_home_lights;
    private static int battery_soc_lights;
    private static double destination_lat;
    private static double destination_lon;
    private static int start_buzzer_sequence;
    private static int voice_controll;

    //<-------------GETTERS
    public static int getBattery_left_distance() {
        return battery_left_distance;
    }
    public static int getBattery_state_of_charge() {
        return battery_state_of_charge;
    }
    public static int getBattery_voltage() {
        return battery_voltage;
    }
    public static int getBattery_cell_balance() {
        return battery_cell_balance;
    }
    public static int getBattery_temperature() {
        return battery_temperature;
    }
    public static int getBattery_charge_cycles() {
        return battery_charge_cycles;
    }
    public static int getBattery_time_to_full_charge() {
        return battery_time_to_full_charge;
    }
    public static int getBattery_charging_amps() {
        return battery_charging_amps;
    }
    public static boolean isCharging() {
        return charging;
    }

    public static int getDriving_mode() {
        return driving_mode;
    }
    public static int getCustom_max_speed() {
        return custom_max_speed;
    }
    public static int getCustom_max_torque() {
        return custom_max_torque;
    }
    public static int getCustom_brake_regeneration() {
        return custom_brake_regeneration;
    }

    public static int getMilleage() {
        return milleage;
    }
    public static int getRegistered_max_speed() {
        return registered_max_speed;
    }
    public static int getRegistered_max_angle() {
        return registered_max_angle;
    }
    public static int getRegistered_max_torque() {
        return registered_max_torque;
    }
    public static int getEngine_temperature() {
        return engine_temperature;
    }
    public static int getOdometer() {
        return odometer;
    }

    public static int isAuto_lights() {
        return auto_lights;
    }
    public static int isBack_home_lights() {
        return back_home_lights;
    }
    public static int isBattery_soc_lights() {
        return battery_soc_lights;
    }
    public static int isStart_buzzer_sequence() {
        return start_buzzer_sequence;
    }
    public static int isVoice_controll() {
        return voice_controll;
    }
    public static double getDestination_lat() {
        return destination_lat;
    }
    public static double getDestination_lon() {
        return destination_lon;
    }

    //<-------------SETTERS
    public static void setBattery_left_distance(int battery_left_distance) {
        MotoParameters.battery_left_distance = battery_left_distance < 0 ? 0 : battery_left_distance;
    }
    public static void setBattery_cell_balance(int battery_cell_balance) {
        MotoParameters.battery_cell_balance = battery_cell_balance < 0 ? 0 : battery_cell_balance;
    }
    public static void setBattery_charge_cycles(int battery_charge_cycles) {
        MotoParameters.battery_charge_cycles = battery_charge_cycles < 0 ? 0 : battery_charge_cycles;
    }
    public static void setBattery_charging_amps(int battery_charging_amps) {
        MotoParameters.battery_charging_amps = battery_charging_amps < 0 ? 0 : battery_charging_amps;
    }
    public static void setBattery_state_of_charge(int battery_state_of_charge) {
        MotoParameters.battery_state_of_charge = battery_state_of_charge < 0 ? 0 : battery_state_of_charge;
    }
    public static void setBattery_temperature(int battery_temperature) {
        MotoParameters.battery_temperature = battery_temperature < 0 ? 0 : battery_temperature;
    }
    public static void setBattery_time_to_full_charge(int battery_time_to_full_charge) {
        MotoParameters.battery_time_to_full_charge = battery_time_to_full_charge < 0 ? 0 : battery_time_to_full_charge;
    }
    public static void setBattery_voltage(int battery_voltage) {
        MotoParameters.battery_voltage = battery_voltage < 0 ? 0 : battery_voltage;
    }
    public static void setCharging(boolean charging) {
        MotoParameters.charging = charging;
    }

    public static void setDriving_mode(int driving_mode) {
        if(driving_mode == 1 || driving_mode == 2 || driving_mode == 3 || driving_mode == 4)
        {
            MotoParameters.driving_mode = driving_mode;
        }
        else
        {
            Log.e("MotoParameters ", "Incorrect driving mode. Set to ECO");
            MotoParameters.driving_mode = 0;
        }
    }
    public static void setCustom_max_speed(int custom_max_speed) {
        MotoParameters.custom_max_speed = custom_max_speed < 1 ? 1 : custom_max_speed;
    }
    public static void setCustom_max_torque(int custom_max_torque) {
        MotoParameters.custom_max_torque = custom_max_torque < 1 ? 1 : custom_max_torque;
    }
    public static void setCustom_brake_regeneration(int custom_brake_regeneration) {
        MotoParameters.custom_brake_regeneration = custom_brake_regeneration < 1 ? 1 : custom_brake_regeneration;
    }

    public static void setMilleage(int milleage) {
        MotoParameters.milleage = milleage < 0 ? 0 : milleage;
    }
    public static void setEngine_temperature(int engine_temperature) {
        MotoParameters.engine_temperature = engine_temperature < 0 ? 0 : engine_temperature;
    }
    public static void setOdometer(int odometer) {
        MotoParameters.odometer = odometer < 0 ? 0 : odometer;
    }
    public static void setRegistered_max_angle(int registered_max_angle) {
        MotoParameters.registered_max_angle = registered_max_angle < 0 ? 0 : registered_max_angle;
    }
    public static void setRegistered_max_speed(int registered_max_speed) {
        MotoParameters.registered_max_speed = registered_max_speed < 0 ? 0 : registered_max_speed;
    }
    public static void setRegistered_max_torque(int registered_max_torque) {
        MotoParameters.registered_max_torque = registered_max_torque < 0 ? 0 : registered_max_torque;
    }

    public static void setAuto_lights(int auto_lights) {
        MotoParameters.auto_lights = auto_lights;
    }
    public static void setBack_home_lights(int back_home_lights) {
        MotoParameters.back_home_lights = back_home_lights;
    }
    public static void setBattery_soc_lights(int battery_soc_lights) {
        MotoParameters.battery_soc_lights = battery_soc_lights;
    }
    public static void setStart_buzzer_sequence(int start_buzzer_sequence) {
        MotoParameters.start_buzzer_sequence = start_buzzer_sequence;
    }
    public static void setVoice_controll(int voice_controll) {
        MotoParameters.voice_controll = voice_controll;
    }
    public static void setDestination_lat(double destination_lat) {
        MotoParameters.destination_lat = destination_lat < 0 ? 0 : destination_lat;;
    }
    public static void setDestination_lon(double destination_lon) {
        MotoParameters.destination_lon = destination_lon < 0 ? 0 : destination_lon;;
    }

    //<-------------Functions
    public static void setupDefaultParamateres()
    {
        //battery
        setBattery_state_of_charge(0);
        setBattery_left_distance(0);
        setBattery_cell_balance(0);
        setBattery_voltage(0);
        setBattery_temperature(0);
        setBattery_charge_cycles(0);
        setBattery_time_to_full_charge(0);
        setBattery_charging_amps(0);
        setCharging(false);
        //performance
        setDriving_mode(1);
        setCustom_max_speed(1);
        setCustom_max_torque(1);
        setCustom_brake_regeneration(1);
        //drivetrain
        setMilleage(0);
        setRegistered_max_speed(0);
        setRegistered_max_angle(0);
        setRegistered_max_torque(0);
        setOdometer(0);
        setEngine_temperature(0);
        //settings
        setAuto_lights(0);
        setBack_home_lights(0);
        setBattery_soc_lights(0);
        setDestination_lat(0);
        setDestination_lon(0);
        setStart_buzzer_sequence(0);
        setVoice_controll(0);
    }
}
