package com.mobile.smartconnectdashboard;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.util.List;

public class SettingsFragment extends Fragment {

    TextView destinationTextEdit;
    Button setDestinationButton;
    Button startNavigationButton;
    CheckBox autoLightsCheckBox;
    CheckBox backHomeLightsCheckBox;
    CheckBox batterySOCLightsCheckBox;
    CheckBox startBuzzerSequenceCheckBox;
    CheckBox voiceControllCheckBox;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_settings, container, false);
        destinationTextEdit = (EditText)view.findViewById(R.id.editTextNavigation);
        setDestinationButton = (Button) view.findViewById(R.id.buttonSetDestination);
        startNavigationButton = (Button) view.findViewById(R.id.buttonNavigation);
        autoLightsCheckBox = (CheckBox)view.findViewById(R.id.autoLightsCheckBox);
        backHomeLightsCheckBox = (CheckBox)view.findViewById(R.id.backHomeLightsCheckBox);
        batterySOCLightsCheckBox = (CheckBox)view.findViewById(R.id.batterySOCLights);
        startBuzzerSequenceCheckBox = (CheckBox)view.findViewById(R.id.startBuzzerCheckBox);
        voiceControllCheckBox = (CheckBox)view.findViewById(R.id.voiceControllCheckBox);
        updateParameters();

        startNavigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] msg = {0,0,0,0,0,0,0,0};

                msg[0] = MotoParameters.MSG_START[0];
                msg[1] = MotoParameters.MSG_START[1];
                msg[2] = MotoParameters.ACTIONS_ID;
                msg[3] = 0x1;
                msg[4] = 0;
                msg[5] = 0;
                msg[6] = 0;
                msg[7] = 0;

                try {
                    LoginActivity.bt_ConnectedThread.writeBytes(msg);
                    if(LoginActivity.DEBUG)
                    {
                        Log.d("ACTION msg[0]", String.valueOf(msg[0]));
                        Log.d("ACTION msg[1]", String.valueOf(msg[1]));
                        Log.d("ACTION msg[2]", String.valueOf(msg[2]));
                        Log.d("ACTION msg[3]", String.valueOf(msg[3]));
                        Log.d("ACTION msg[4]", String.valueOf(msg[4]));
                        Log.d("ACTION msg[5]", String.valueOf(msg[5]));
                        Log.d("ACTION msg[6]", String.valueOf(msg[6]));
                        Log.d("ACTION msg[7]", String.valueOf(msg[7]));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        setDestinationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String destination = destinationTextEdit.getText().toString();
                if(destination.length()>1)
                {
                    try {
                        LatLng coords = getLocationFromAddress(view.getContext(), destination);
                        int decimalValLat = calculateDecimalSize((int)coords.latitude);
                        int decimalValLon = calculateDecimalSize((int)coords.longitude);
                        long lat = (long) (coords.latitude *10000000);
                        long lon = (long) (coords.longitude *10000000);

                        byte[] msg_lat = {0,0,0,0,0,0,0,0};
                        byte[] msg_lon = {0,0,0,0,0,0,0,0};

                        msg_lat[0] = MotoParameters.START_COORDINATES_MESSAGE;
                        msg_lat[1] = MotoParameters.LATTITUDE_ID;
                        msg_lat[2] = (byte) (lat);
                        msg_lat[3] = (byte) (lat >> 8);
                        msg_lat[4] = (byte) (lat >> 16);
                        msg_lat[5] = (byte) (lat >> 24);
                        msg_lat[6] = (byte) (lat >> 32);
                        msg_lat[7] = (byte) (decimalValLat);

                        msg_lon[0] = MotoParameters.START_COORDINATES_MESSAGE;
                        msg_lon[1] = MotoParameters.LONGITUDE_ID;
                        msg_lon[2] = (byte) (lon);
                        msg_lon[3] = (byte) (lon >> 8);
                        msg_lon[4] = (byte) (lon >> 16);
                        msg_lon[5] = (byte) (lon >> 24);
                        msg_lon[6] = (byte) (lon >> 32);
                        msg_lon[7] = (byte) (decimalValLon);

                        if(LoginActivity.DEBUG)
                        {
                            Log.d("LAT msg[0]", String.valueOf((int)msg_lat[0]&0xFF));
                            Log.d("LAT msg[1]", String.valueOf((int)msg_lat[1]&0xFF));
                            Log.d("LAT msg[2]", String.valueOf((int)msg_lat[2]&0xFF));
                            Log.d("LAT msg[3]", String.valueOf((int)msg_lat[3]&0xFF));
                            Log.d("LAT msg[4]", String.valueOf((int)msg_lat[4]&0xFF));
                            Log.d("LAT msg[5]", String.valueOf((int)msg_lat[5]&0xFF));
                            Log.d("LAT msg[6]", String.valueOf((int)msg_lat[6]&0xFF));
                            Log.d("LAT msg[7]", String.valueOf((int)msg_lat[7]&0xFF));
                            Log.d("SEPARATOR", "---------------");
                            Log.d("LON msg[0]", String.valueOf((int)msg_lon[0]&0xFF));
                            Log.d("LON msg[1]", String.valueOf((int)msg_lon[1]&0xFF));
                            Log.d("LON msg[2]", String.valueOf((int)msg_lon[2]&0xFF));
                            Log.d("LON msg[3]", String.valueOf((int)msg_lon[3]&0xFF));
                            Log.d("LON msg[4]", String.valueOf((int)msg_lon[4]&0xFF));
                            Log.d("LON msg[5]", String.valueOf((int)msg_lon[5]&0xFF));
                            Log.d("LON msg[6]", String.valueOf((int)msg_lon[6]&0xFF));
                            Log.d("LON msg[7]", String.valueOf((int)msg_lon[7]&0xFF));
                        }

                        try {
                            LoginActivity.bt_ConnectedThread.writeBytes(msg_lat);
                            LoginActivity.bt_ConnectedThread.writeBytes(msg_lon);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("SettingsFragment [NAVIGATION] ", "New destination: ("+String.valueOf(coords.latitude)+", "+String.valueOf(coords.longitude)+")");

                    } catch (Exception ex) {

                        Log.e("SettingsFragment [NAVIGATION] ", ex.getMessage());
                    }
                }
            }
        });

        autoLightsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked())
                {
                    MotoParameters.setAuto_lights(1);
                } else {
                    MotoParameters.setAuto_lights(0);
                }
                sendData();
            }
        });

        backHomeLightsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked())
                {
                    MotoParameters.setBack_home_lights(1);
                } else {
                    MotoParameters.setBack_home_lights(0);
                }
                sendData();
            }
        });

        batterySOCLightsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked())
                {
                    MotoParameters.setBattery_soc_lights(1);
                } else {
                    MotoParameters.setBattery_soc_lights(0);
                }
                sendData();
            }
        });

        startBuzzerSequenceCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked())
                {
                    MotoParameters.setStart_buzzer_sequence(1);
                } else {
                    MotoParameters.setStart_buzzer_sequence(0);
                }
                sendData();
            }
        });

        voiceControllCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked())
                {
                    MotoParameters.setVoice_controll(1);
                } else {
                    MotoParameters.setVoice_controll(0);
                }
                sendData();
            }
        });

        return view;
    }

    public LatLng getLocationFromAddress(Context context,String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng LatLan= null;
        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            LatLan= new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return LatLan;
    }

    private void updateParameters()
    {
        //check lights
        if(MotoParameters.isAuto_lights() == 1)
        {
            autoLightsCheckBox.setChecked(true);
        }
        else
        {
            autoLightsCheckBox.setChecked(false);
        }

        if(MotoParameters.isBack_home_lights() == 1)
        {
            backHomeLightsCheckBox.setChecked(true);
        }
        else
        {
            backHomeLightsCheckBox.setChecked(false);
        }

        if(MotoParameters.isBattery_soc_lights() == 1)
        {
            batterySOCLightsCheckBox.setChecked(true);
        }
        else
        {
            batterySOCLightsCheckBox.setChecked(false);
        }

        //check others
        if(MotoParameters.isStart_buzzer_sequence() == 1)
        {
            startBuzzerSequenceCheckBox.setChecked(true);
        }
        else
        {
            startBuzzerSequenceCheckBox.setChecked(false);
        }

        if(MotoParameters.isVoice_controll() == 1)
        {
            voiceControllCheckBox.setChecked(true);
        }
        else
        {
            voiceControllCheckBox.setChecked(false);
        }
    }

    private void sendData()
    {
        byte[] msg = {0,0,0,0,0,0,0,0};
        msg[0] = MotoParameters.MSG_START[0];
        msg[1] = MotoParameters.MSG_START[1];
        msg[2] = MotoParameters.SETTINGS_ID;
        msg[3] = (byte) (MotoParameters.isAuto_lights());
        msg[4] = (byte) (MotoParameters.isBack_home_lights());
        msg[5] = (byte) (MotoParameters.isBattery_soc_lights());
        msg[6] = (byte) (MotoParameters.isStart_buzzer_sequence());
        msg[7] = (byte) (MotoParameters.isVoice_controll());
        try {
            LoginActivity.bt_ConnectedThread.writeBytes(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int calculateDecimalSize(int value)
    {
        if(value > 0 && value<10)
        {
            value = 1;
        }
        else if(value >=10 && value <100)
        {
            value = 2;
        }
        else if(value >= 100 && value <=180)
        {
            value = 3;
        }
        return value;
    }
}
