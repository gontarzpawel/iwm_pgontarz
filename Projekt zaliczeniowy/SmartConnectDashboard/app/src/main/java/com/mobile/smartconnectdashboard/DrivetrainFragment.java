package com.mobile.smartconnectdashboard;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DrivetrainFragment extends Fragment {

    TextView milleageTextView;
    TextView maxSpeedTextView;
    TextView odomterTextView;
    TextView engineTemperatureTextView;
    TextView maxAngleTextView;
    TextView maxTorqueTextView;
    Handler handler = new Handler();

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            updateParameters();
            handler.postDelayed(this, 1000);
        }
    };

    public static DrivetrainFragment newInstance() {
        DrivetrainFragment fragment = new DrivetrainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_drivetrain, container, false);
        milleageTextView = (TextView) view.findViewById(R.id.milleageTextView);
        maxSpeedTextView = (TextView) view.findViewById(R.id.maxSpeedTextView);
        odomterTextView = (TextView) view.findViewById(R.id.odometerTextView);
        engineTemperatureTextView = (TextView) view.findViewById(R.id.engineTempTextView);
        maxAngleTextView = (TextView) view.findViewById(R.id.maxAngleTextView);
        maxTorqueTextView = (TextView) view.findViewById(R.id.maxTorqueTextView);

        updateParameters();
        handler.post(runnableCode);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(runnableCode);
        handler.removeCallbacks(runnableCode);
    }

    private void updateParameters()
    {
        milleageTextView.setText(String.valueOf(MotoParameters.getMilleage()));
        maxSpeedTextView.setText(String.valueOf(MotoParameters.getRegistered_max_speed()));
        odomterTextView.setText(String.valueOf(MotoParameters.getOdometer()));
        engineTemperatureTextView.setText(String.valueOf(MotoParameters.getEngine_temperature()));
        maxAngleTextView.setText(String.valueOf(MotoParameters.getRegistered_max_angle()));
        maxTorqueTextView.setText(String.valueOf(MotoParameters.getRegistered_max_torque()));
    }
}