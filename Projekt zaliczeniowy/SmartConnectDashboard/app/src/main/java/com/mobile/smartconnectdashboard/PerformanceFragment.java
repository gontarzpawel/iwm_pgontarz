package com.mobile.smartconnectdashboard;
import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class PerformanceFragment extends Fragment {
    RadioGroup radioGroup;
    RadioButton radioECO;
    RadioButton radioNORMAL;
    RadioButton radioSPORT;
    RadioButton radioCUSTOM;

    SeekBar maxSpeedSeekBar;
    SeekBar maxTorqueSeekBar;
    SeekBar brakeRegenerationSeekBar;

    TextView maxSpeedTextView;
    TextView maxTorqueTextView;
    TextView brakeRegenerationTextView;

    public static PerformanceFragment newInstance() {
        PerformanceFragment fragment = new PerformanceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_performance, container, false);

        radioGroup = (RadioGroup)view.findViewById(R.id.drivingModesRadioGroup);
        radioECO = (RadioButton)view.findViewById(R.id.radioECO);
        radioNORMAL = (RadioButton)view.findViewById(R.id.radioNORMAL);
        radioSPORT = (RadioButton)view.findViewById(R.id.radioSPORT);
        radioCUSTOM = (RadioButton)view.findViewById(R.id.radioCUSTOM);

        maxSpeedSeekBar = (SeekBar)view.findViewById(R.id.maxSpeedSeekBar);
        maxTorqueSeekBar = (SeekBar)view.findViewById(R.id.maxTorqueSeekBar);
        brakeRegenerationSeekBar = (SeekBar)view.findViewById(R.id.brakeRegenerationSeekBar);

        maxSpeedTextView = (TextView)view.findViewById(R.id.maxSpeedTextView);
        maxTorqueTextView = (TextView)view.findViewById(R.id.maxTorqueTextView);
        brakeRegenerationTextView = (TextView)view.findViewById(R.id.brakeRegenerationTextView);

        updateParameters();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)view.findViewById(checkedId);
                if(radioECO.isChecked())
                {
                    MotoParameters.setDriving_mode(1);
                    checkSeekBars(MotoParameters.getDriving_mode());

                    byte[] msg = {0,0,0,0,0,0,0,0};
                    msg[0] = MotoParameters.MSG_START[0];
                    msg[1] = MotoParameters.MSG_START[1];
                    msg[2] = MotoParameters.PERFORMANCE_ID;
                    msg[3] = (byte) (MotoParameters.getDriving_mode());
                    msg[4] = (byte) (MotoParameters.max_speed_eco);
                    msg[5] = (byte) (MotoParameters.max_torque_eco);
                    msg[6] = (byte) (MotoParameters.brake_regeneration_eco);
                    msg[7] = 0;
                    try {
                        LoginActivity.bt_ConnectedThread.writeBytes(msg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(radioNORMAL.isChecked())
                {
                    MotoParameters.setDriving_mode(2);
                    checkSeekBars(MotoParameters.getDriving_mode());

                    byte[] msg = {0,0,0,0,0,0,0,0};
                    msg[0] = MotoParameters.MSG_START[0];
                    msg[1] = MotoParameters.MSG_START[1];
                    msg[2] = MotoParameters.PERFORMANCE_ID;
                    msg[3] = (byte) (MotoParameters.getDriving_mode());
                    msg[4] = (byte) (MotoParameters.max_speed_normal);
                    msg[5] = (byte) (MotoParameters.max_torque_normal);
                    msg[6] = (byte) (MotoParameters.brake_regeneration_normal);
                    msg[7] = 0;
                    try {
                        LoginActivity.bt_ConnectedThread.writeBytes(msg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                if(radioSPORT.isChecked())
                {
                    MotoParameters.setDriving_mode(3);
                    checkSeekBars(MotoParameters.getDriving_mode());

                    byte[] msg = {0,0,0,0,0,0,0,0};
                    msg[0] = MotoParameters.MSG_START[0];
                    msg[1] = MotoParameters.MSG_START[1];
                    msg[2] = MotoParameters.PERFORMANCE_ID;
                    msg[3] = (byte) (MotoParameters.getDriving_mode());
                    msg[4] = (byte) (MotoParameters.max_speed_sport);
                    msg[5] = (byte) (MotoParameters.max_torque_sport);
                    msg[6] = (byte) (MotoParameters.brake_regeneration_sport);
                    msg[7] = 0;
                    try {
                        LoginActivity.bt_ConnectedThread.writeBytes(msg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                if(radioCUSTOM.isChecked())
                {
                    MotoParameters.setDriving_mode(4);
                    checkSeekBars(MotoParameters.getDriving_mode());

                    byte[] msg = {0,0,0,0,0,0,0,0};
                    msg[0] = MotoParameters.MSG_START[0];
                    msg[1] = MotoParameters.MSG_START[1];
                    msg[2] = MotoParameters.PERFORMANCE_ID;
                    msg[3] = (byte) (MotoParameters.getDriving_mode());
                    msg[4] = (byte) (MotoParameters.max_speed_sport);
                    msg[5] = (byte) (MotoParameters.max_torque_sport);
                    msg[6] = (byte) (MotoParameters.brake_regeneration_sport);
                    msg[7] = 0;
                    try {
                        LoginActivity.bt_ConnectedThread.writeBytes(msg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        maxSpeedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MotoParameters.setCustom_max_speed(seekBar.getProgress());
                MotoParameters.setDriving_mode(4);
                MotoParameters.setCustom_max_speed(maxSpeedSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_brake_regeneration((brakeRegenerationSeekBar.getProgress()));
                sendData();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                maxSpeedTextView.setText(String.valueOf(progress));
            }
        });

        maxTorqueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxTorqueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MotoParameters.setCustom_max_torque(seekBar.getProgress());
                MotoParameters.setDriving_mode(4);
                MotoParameters.setCustom_max_speed(maxSpeedSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_brake_regeneration((brakeRegenerationSeekBar.getProgress()));
                sendData();
            }
        });

        brakeRegenerationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                brakeRegenerationTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MotoParameters.setCustom_brake_regeneration(seekBar.getProgress());
                MotoParameters.setDriving_mode(4);
                MotoParameters.setCustom_max_speed(maxSpeedSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_max_torque(maxTorqueSeekBar.getProgress());
                MotoParameters.setCustom_brake_regeneration((brakeRegenerationSeekBar.getProgress()));
                sendData();
            }
        });


        return view;
    }
    private void toogleSeekBars(boolean state)
    {
        if(state==true)
        {
            maxSpeedSeekBar.setAlpha(1.0F);
            maxTorqueSeekBar.setAlpha(1.0F);
            brakeRegenerationSeekBar.setAlpha(1.0F);

            maxSpeedSeekBar.setEnabled(true);
            maxTorqueSeekBar.setEnabled(true);
            brakeRegenerationSeekBar.setEnabled(true);
        }
        else
        {
            maxSpeedSeekBar.setAlpha(0.7F);
            maxTorqueSeekBar.setAlpha(0.7F);
            brakeRegenerationSeekBar.setAlpha(0.7F);

            maxSpeedSeekBar.setEnabled(false);
            maxTorqueSeekBar.setEnabled(false);
            brakeRegenerationSeekBar.setEnabled(false);
        }
    }

    private void checkSeekBars(int driving_mode)
    {
        if(driving_mode == 1)
        {

            radioECO.setTextColor(getResources().getColorStateList(R.color.triggerColor));
            radioECO.setButtonTintList(getResources().getColorStateList(R.color.triggerColor));//set the color tint list
            radioNORMAL.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioECO.invalidate();
            radioNORMAL.invalidate();
            radioSPORT.invalidate();
            radioCUSTOM.invalidate();
            toogleSeekBars(false);

            maxSpeedSeekBar.setProgress(MotoParameters.max_speed_eco);
            maxSpeedTextView.setText(String.valueOf(maxSpeedSeekBar.getProgress()));
            maxTorqueSeekBar.setProgress(MotoParameters.max_torque_eco);
            maxTorqueTextView.setText(String.valueOf(maxTorqueSeekBar.getProgress()));
            brakeRegenerationSeekBar.setProgress(MotoParameters.brake_regeneration_eco);
            brakeRegenerationTextView.setText(String.valueOf(brakeRegenerationSeekBar.getProgress()));

            radioECO.setChecked(true);
            radioNORMAL.setChecked(false);
            radioSPORT.setChecked(false);
            radioCUSTOM.setChecked(false);
        }
        else if(driving_mode == 2)
        {
            radioNORMAL.setTextColor(getResources().getColorStateList(R.color.triggerColor));
            radioNORMAL.setButtonTintList(getResources().getColorStateList(R.color.triggerColor));//set the color tint list
            radioECO.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioECO.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.invalidate();
            radioECO.invalidate();
            radioSPORT.invalidate();
            radioCUSTOM.invalidate();
            toogleSeekBars(false);

            maxSpeedSeekBar.setProgress(MotoParameters.max_speed_normal);
            maxSpeedTextView.setText(String.valueOf(maxSpeedSeekBar.getProgress()));
            maxTorqueSeekBar.setProgress(MotoParameters.max_torque_normal);
            maxTorqueTextView.setText(String.valueOf(maxTorqueSeekBar.getProgress()));
            brakeRegenerationSeekBar.setProgress(MotoParameters.brake_regeneration_normal);
            brakeRegenerationTextView.setText(String.valueOf(brakeRegenerationSeekBar.getProgress()));

            radioECO.setChecked(false);
            radioNORMAL.setChecked(true);
            radioSPORT.setChecked(false);
            radioCUSTOM.setChecked(false);
        }
        else if(driving_mode == 3)
        {
            radioSPORT.setTextColor(getResources().getColorStateList(R.color.triggerColor));
            radioSPORT.setButtonTintList(getResources().getColorStateList(R.color.triggerColor));//set the color tint list
            radioECO.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioECO.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.invalidate();
            radioECO.invalidate();
            radioNORMAL.invalidate();
            radioCUSTOM.invalidate();
            toogleSeekBars(false);

            maxSpeedSeekBar.setProgress(MotoParameters.max_speed_sport);
            maxSpeedTextView.setText(String.valueOf(maxSpeedSeekBar.getProgress()));
            maxTorqueSeekBar.setProgress(MotoParameters.max_torque_sport);
            maxTorqueTextView.setText(String.valueOf(maxTorqueSeekBar.getProgress()));
            brakeRegenerationSeekBar.setProgress(MotoParameters.brake_regeneration_sport);
            brakeRegenerationTextView.setText(String.valueOf(brakeRegenerationSeekBar.getProgress()));

            radioECO.setChecked(false);
            radioNORMAL.setChecked(false);
            radioSPORT.setChecked(true);
            radioCUSTOM.setChecked(false);
        }
        else if(driving_mode == 4)
        {
            radioCUSTOM.setTextColor(getResources().getColorStateList(R.color.triggerColor));
            radioCUSTOM.setButtonTintList(getResources().getColorStateList(R.color.triggerColor));//set the color tint list
            radioECO.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setTextColor(getResources().getColorStateList(R.color.defaultModeColor));
            radioECO.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioNORMAL.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioSPORT.setButtonTintList(getResources().getColorStateList(R.color.defaultModeColor));
            radioCUSTOM.invalidate();
            radioECO.invalidate();
            radioNORMAL.invalidate();
            radioSPORT.invalidate();
            toogleSeekBars(true);

            maxSpeedSeekBar.setProgress(MotoParameters.getCustom_max_speed());
            maxSpeedTextView.setText(String.valueOf(maxSpeedSeekBar.getProgress()));
            maxTorqueSeekBar.setProgress(MotoParameters.getCustom_max_torque());
            maxTorqueTextView.setText(String.valueOf(maxTorqueSeekBar.getProgress()));
            brakeRegenerationSeekBar.setProgress(MotoParameters.getCustom_brake_regeneration());
            brakeRegenerationTextView.setText(String.valueOf(brakeRegenerationSeekBar.getProgress()));

            radioECO.setChecked(false);
            radioNORMAL.setChecked(false);
            radioSPORT.setChecked(false);
            radioCUSTOM.setChecked(true);

        }
        else
        {

        }
    }

    private void updateParameters()
    {
        //check driving_mode
        checkSeekBars(MotoParameters.getDriving_mode());
    }

    private void sendData()
    {
        byte[] msg = {0,0,0,0,0,0,0,0};
        msg[0] = MotoParameters.MSG_START[0];
        msg[1] = MotoParameters.MSG_START[1];
        msg[2] = MotoParameters.PERFORMANCE_ID;
        msg[3] = (byte) (MotoParameters.getDriving_mode());
        msg[4] = (byte) (MotoParameters.getCustom_max_speed());
        msg[5] = (byte) (MotoParameters.getCustom_max_torque());
        msg[6] = (byte) (MotoParameters.getCustom_brake_regeneration());
        msg[7] = 0;
        try {
            LoginActivity.bt_ConnectedThread.writeBytes(msg);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}