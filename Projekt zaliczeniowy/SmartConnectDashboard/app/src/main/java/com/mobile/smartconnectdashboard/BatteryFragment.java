package com.mobile.smartconnectdashboard;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BatteryFragment extends Fragment {

    TextView leftDistanceTextView;
    TextView stateOfChargeTextView;
    TextView batteryVoltageTextView;
    TextView cellBallanceTextView;
    TextView batteryTemperatureTextView;
    TextView chargeCyclesTextView;
    TextView timeToFullChargeTextView;
    TextView chargingAmpsTextView;
    Handler handler = new Handler();

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            updateParameters();
            handler.postDelayed(this, 1000);
        }
    };


    public static BatteryFragment newInstance() {
        BatteryFragment fragment = new BatteryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_battery, container, false);

        leftDistanceTextView = (TextView)view.findViewById(R.id.leftDistanceTextView);
        stateOfChargeTextView = (TextView)view.findViewById(R.id.batterySOCTextView);
        batteryVoltageTextView = (TextView)view.findViewById(R.id.batteryVoltageTextView);
        cellBallanceTextView = (TextView)view.findViewById(R.id.cellBalanceTextView);
        batteryTemperatureTextView = (TextView)view.findViewById(R.id.batteryTemperatureTextView);
        chargeCyclesTextView = (TextView)view.findViewById(R.id.chargeCyclesTextView);
        timeToFullChargeTextView = (TextView)view.findViewById(R.id.timeToFullChargeTextView);
        chargingAmpsTextView = (TextView)view.findViewById(R.id.chargingAmpsTextView);

        updateParameters();
        handler.post(runnableCode);
        return view;
    }

    private void updateParameters()
    {
        leftDistanceTextView.setText(String.valueOf(MotoParameters.getBattery_left_distance()));
        stateOfChargeTextView.setText(String.valueOf(MotoParameters.getBattery_state_of_charge()));
        batteryVoltageTextView.setText(String.valueOf(MotoParameters.getBattery_voltage()));
        cellBallanceTextView.setText(String.valueOf(MotoParameters.getBattery_cell_balance()));
        batteryTemperatureTextView.setText(String.valueOf(MotoParameters.getBattery_temperature()));
        chargeCyclesTextView.setText(String.valueOf(MotoParameters.getBattery_charge_cycles()));
        timeToFullChargeTextView.setText(String.valueOf(MotoParameters.getBattery_time_to_full_charge()));
        chargingAmpsTextView.setText(String.valueOf(MotoParameters.getBattery_charging_amps()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(runnableCode);
        handler.removeCallbacks(runnableCode);
    }
}