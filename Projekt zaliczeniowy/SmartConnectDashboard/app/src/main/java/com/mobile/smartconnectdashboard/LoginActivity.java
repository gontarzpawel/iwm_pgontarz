package com.mobile.smartconnectdashboard;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class LoginActivity extends AppCompatActivity {

    ImageButton discoverNewDevicesButton;
    ProgressBar bluetoothProgressBar;
    TextView loginActivityTextView;

    private BluetoothAdapter mBTAdapter;
    private Set<BluetoothDevice> mPairedDevices;
    private ArrayAdapter<String> mBTArrayAdapter;
    public static final boolean DEBUG = false;

    private ListView mDevicesListView;
    private final String TAG = LoginActivity.class.getSimpleName();
    private Toast toastMessage;

    public static boolean isBluetoothConnected = false;
    public static Handler bt_Handler; //handler that will receive callback notifications
    public static ConnectedThread bt_ConnectedThread; // bluetooth background worker thread to send and receive data
    public static BluetoothSocket BTSocket = null; // bi-directional client-to-client data path

    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // "random" unique identifier

    // #defines for identifying shared types between calling functions
    private final static int REQUEST_ENABLE_BT = 1; // used to identify adding bluetooth names
    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3; // used in bluetooth handler to identify message status

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //setDefaultParameters
        MotoParameters.setupDefaultParamateres();

        mBTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1)
        {
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);
                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.CYAN);
                // Generate ListView Item using TextView
                return view;
            }
        };

        mBTAdapter = BluetoothAdapter.getDefaultAdapter(); // get a handle on the bluetooth radio

        mDevicesListView = (ListView)findViewById(R.id.devicesListView);
        mDevicesListView.setAdapter(mBTArrayAdapter); // assign model to view
        mDevicesListView.setOnItemClickListener(mDeviceClickListener);

        discoverNewDevicesButton = (ImageButton)findViewById(R.id.btDiscoverButton);
        discoverNewDevicesButton.setImageAlpha(130);
        discoverNewDevicesButton.setImageDrawable(getResources().getDrawable(R.drawable.bt_icon));

        bluetoothProgressBar = (ProgressBar)findViewById(R.id.bluetoothProgressBar);
        bluetoothProgressBar.setVisibility(View.INVISIBLE);

        loginActivityTextView = (TextView)findViewById(R.id.loginActivityTextView);
        loginActivityTextView.setText("Find motorcycle");

        // Ask for location permission if not already allowed
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        //check bluetooth status and turn on bluetooth
        if (mBTArrayAdapter == null) {
            // Device does not support Bluetooth
            //mBluetoothStatus.setText("Status: Bluetooth not found");
            if (toastMessage!= null) {
                toastMessage.cancel();
            }
            toastMessage= Toast.makeText(getApplicationContext(), "Bluetooth device not found!", Toast.LENGTH_SHORT);
            toastMessage.show();
        }
        else {

            if (!mBTAdapter.isEnabled()) {
                mBTArrayAdapter.clear(); // clear items
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        bt_Handler = new Handler(){
            public void handleMessage(android.os.Message msg){
                if(msg.what == MESSAGE_READ){
                    String readMessage = null;
                    try {
                        readMessage = new String((byte[])msg.obj, "US-ASCII");
                        byte[] rawBytes = (byte[])msg.obj;

                        if(msg.arg1 == MotoParameters.MSG_LENGHT)
                        {
                            if(DEBUG)
                            {
                                Log.d("BLUETOOTH_HANDLER ", "Receive " + String.valueOf(msg.arg1) + " bytes, message: "+ String.valueOf(msg.obj));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[0]: ", String.valueOf((int)rawBytes[0]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[1]: ", String.valueOf((int)rawBytes[1]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[2]: ", String.valueOf((int)rawBytes[2]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[3]: ", String.valueOf((int)rawBytes[3]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[4]: ", String.valueOf((int)rawBytes[4]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[5]: ", String.valueOf((int)rawBytes[5]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[6]: ", String.valueOf((int)rawBytes[6]&0xFF));
                                Log.d("BLUETOOTH_HANDLER MESSAGE[7]: ", String.valueOf((int)rawBytes[7]&0xFF));
                            }
                            decodeReceiveData(rawBytes);
                        }
                        else
                        {
                            Log.e("BLUETOOTH_HANDLER ", "Incorrect msg lenght: "+msg.arg1);
                        }

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                if(msg.what == CONNECTING_STATUS){
                    if(msg.arg1 == 1) {

                        bluetoothProgressBar.setVisibility(View.VISIBLE);
                        discoverNewDevicesButton.setEnabled(false);
                        discoverNewDevicesButton.setImageDrawable(getResources().getDrawable(R.drawable.acces));
                        loginActivityTextView.setText("Connected");
                        //mBluetoothStatus.setText("Connected to Device: " + (String)(msg.obj));
                        if (toastMessage!= null) {
                            toastMessage.cancel();
                        }
                        isBluetoothConnected = true;
                        //go to MainActivity\
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                bluetoothProgressBar.setVisibility(View.INVISIBLE);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                        }, 1500);
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final BroadcastReceiver btReceiver = new BroadcastReceiver() {
            @SuppressLint("MissingPermission")
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                String word = "HC";
                if(BluetoothDevice.ACTION_FOUND.equals(action)){
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    //check name and add to the list
                    if(device.getName() != null) {
                        String text = device.getName();
                        if (text.contains(word) == true) {
                            mBTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                            //mBTArrayAdapter.add(device.getName());
                            mBTArrayAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        };

        discoverNewDevicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check if the device is already discovering
                if(mBTAdapter.isDiscovering()){
                    discoverNewDevicesButton.setImageAlpha(130);
                    bluetoothProgressBar.setVisibility(View.INVISIBLE);
                    mBTAdapter.cancelDiscovery();
                    loginActivityTextView.setText("Find device");
                }
                else{
                    if(mBTAdapter.isEnabled()) {
                        discoverNewDevicesButton.setImageAlpha(255);
                        bluetoothProgressBar.setVisibility(View.VISIBLE);
                        mBTArrayAdapter.clear(); // clear items
                        mBTAdapter.startDiscovery();
                        if (toastMessage!= null) {
                            toastMessage.cancel();
                        }
                        loginActivityTextView.setText("Searching");
                        registerReceiver(btReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
                    }
                    else{
                        if (toastMessage!= null) {
                            toastMessage.cancel();
                        }
                        toastMessage= Toast.makeText(getApplicationContext(), "Bluetooth not on", Toast.LENGTH_SHORT);
                        toastMessage.show();
                    }
                }
            }
        });
    }

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        @SuppressLint("MissingPermission")
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            v.setBackgroundColor(getColor(R.color.lightGrey)) ;

            if(!mBTAdapter.isEnabled()) {
                if (toastMessage!= null) {
                    toastMessage.cancel();
                }
                toastMessage= Toast.makeText(getApplicationContext(), "Bluetooth not on", Toast.LENGTH_SHORT);
                toastMessage.show();
                return;
            }
            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            final String address = info.substring(info.length() - 17);
            final String name = info.substring(0,info.length() - 17);

            if (toastMessage!= null) {
                toastMessage.cancel();
            }
            bluetoothProgressBar.setVisibility(View.INVISIBLE);
            loginActivityTextView.setText("Connecting...");

            // Spawn a new thread to avoid blocking the GUI one
            new Thread()
            {
                public void run() {
                    boolean fail = false;

                    BluetoothDevice device = mBTAdapter.getRemoteDevice(address);

                    try {
                        BTSocket = createBluetoothSocket(device);
                    } catch (IOException e) {
                        fail = true;
                        if (toastMessage!= null) {
                            toastMessage.cancel();
                        }
                        toastMessage= Toast.makeText(getApplicationContext(), "Socket creation failed", Toast.LENGTH_SHORT);
                        toastMessage.show();
                    }
                    // Establish the Bluetooth socket connection.
                    try {
                        BTSocket.connect();
                    } catch (IOException e) {
                        try {
                            fail = true;
                            BTSocket.close();
                            bt_Handler.obtainMessage(CONNECTING_STATUS, -1, -1)
                                    .sendToTarget();
                        } catch (IOException e2) {
                            //insert code to deal with this
                            if (toastMessage!= null) {
                                toastMessage.cancel();
                            }
                            toastMessage= Toast.makeText(getApplicationContext(), "Socket creation failed", Toast.LENGTH_SHORT);
                            toastMessage.show();
                        }
                    }
                    if(fail == false) {
                        bt_ConnectedThread = new ConnectedThread(BTSocket);
                        bt_ConnectedThread.start();

                        bt_Handler.obtainMessage(CONNECTING_STATUS, 1, -1, name)
                                .sendToTarget();
                    }
                }
            }.start();
        }
        public void onItemRelease(AdapterView<?> av, View v, int arg2, long arg3) {

        }
    };

    @SuppressLint("MissingPermission")
    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        try {
            final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
            return (BluetoothSocket) m.invoke(device, BTMODULEUUID);
        } catch (Exception e) {
            Log.e(TAG, "Could not create Insecure RFComm Connection",e);
        }
        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    public class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }
        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.available();
                    if(bytes != 0) {
                        buffer = new byte[1024];
                        SystemClock.sleep(200); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        bytes = mmInStream.available(); // how many bytes are ready to be read?
                        //Log.e("mmInStream bytes", String.valueOf(bytes));
                        bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                        bt_Handler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget(); // Send the obtained bytes to the UI activity
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void writeStr(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                //mmOutStream.write(0xB);
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        public void writeByte(Byte b) {
            try {
                mmOutStream.write(b);
            } catch (IOException e) { }
        }

        public void writeBytes(byte[] bytes)
        {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        public void writeInt(int msg) {
            try {
                mmOutStream.write(msg);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                if (toastMessage!= null) {
                    toastMessage.cancel();
                }
                toastMessage= Toast.makeText(getApplicationContext(), "Connection closed.", Toast.LENGTH_SHORT);
                toastMessage.show();
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    private void decodeReceiveData(byte[] message)
    {
        //check correct message structure
        if(((int)message[0]&0xFF) == MotoParameters.MSG_START[0] && ((int)message[1]&0xFF) == MotoParameters.MSG_START[1])
        {
            //switch do data
            switch ((int)message[2]&0xFF)
            {
                case MotoParameters.BATTERY_ENERGY_ID:
                {
                    MotoParameters.setBattery_state_of_charge((int)message[3]&0xFF);
                    MotoParameters.setBattery_voltage((int)message[4]&0xFF);
                    MotoParameters.setBattery_cell_balance((int)message[5]&0xFF);
                    MotoParameters.setBattery_temperature((int)message[6]&0xFF);
                    break;
                }
                case MotoParameters.BATTERY_OVERALL_ID:
                {
                    MotoParameters.setBattery_left_distance((((int)message[4]&0xFF)&0xFF)<<8 | ((int)message[3]&0xFF)&0xFF);
                    MotoParameters.setBattery_charge_cycles((((int)message[6]&0xFF)&0xFF)<<8 | ((int)message[5]&0xFF)&0xFF);
                    break;
                }

                case MotoParameters.BATTERY_CHARGING_ID:
                {
                    if(((int)message[3]&0xFF) == 0)
                    {
                        MotoParameters.setCharging(false);
                    }
                    else
                    {
                        MotoParameters.setCharging(true);
                    }
                    MotoParameters.setBattery_charging_amps((int)message[4]&0xFF);
                    MotoParameters.setBattery_time_to_full_charge((int)message[5]&0xFF);
                    break;
                }

                case MotoParameters.MOTO_OVERALL_ID:
                {
                    MotoParameters.setEngine_temperature((int)message[7]&0xFF);
                    MotoParameters.setMilleage((((int)message[4]&0xFF)&0xFF)<<8 | ((int)message[3]&0xFF)&0xFF);
                    MotoParameters.setOdometer((((int)message[6]&0xFF)&0xFF)<<8 | ((int)message[5]&0xFF)&0xFF);
                    break;
                }

                case MotoParameters.STATS_OVERALL_ID:
                {
                    MotoParameters.setRegistered_max_speed((int)message[3]&0xFF);
                    MotoParameters.setRegistered_max_angle((int)message[4]&0xFF);
                    MotoParameters.setRegistered_max_torque((int)message[5]&0xFF);
                    break;
                }
                case MotoParameters.PERFORMANCE_ID:
                {
                    MotoParameters.setDriving_mode((int)message[3]&0xFF);
                    MotoParameters.setCustom_max_speed((int)message[4]&0xFF);
                    MotoParameters.setCustom_max_torque((int)message[5]&0xFF);
                    MotoParameters.setCustom_brake_regeneration((int)message[6]&0xFF);
                    break;
                }

                case MotoParameters.SETTINGS_ID:
                {
                    MotoParameters.setAuto_lights((int)message[3]&0xFF);
                    MotoParameters.setBack_home_lights((int)message[4]&0xFF);
                    MotoParameters.setBattery_soc_lights((int)message[5]&0xFF);
                    MotoParameters.setStart_buzzer_sequence((int)message[6]&0xFF);
                    MotoParameters.setVoice_controll((int)message[6]&0xFF);
                    break;
                }

                default:
                {
                    System.out.println("default");
                    break;
                }
            }

        }
    }
}