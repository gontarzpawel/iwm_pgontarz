import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server
{
    public static void main(String[] args) {
        try
        {
            Servant servant;
            Registry reg;
            //binding
            reg = LocateRegistry.createRegistry(1099);
            servant = new Servant();
            reg.rebind("Server", servant);
            System.out.println("Data server is ready!");
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }
}
