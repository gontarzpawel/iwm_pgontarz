import java.util.Arrays;

public abstract class Sequence<T> extends Packet
{
    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected T[] buffer;

    public Sequence() {
        super();
        this.channelNr = 0;
        this.unit = "Default unit";
        this.resolution = 0;
        this.buffer = null;
    }

    public Sequence(int channelNr, String unit, double resolution, T[] buffer, String device, String description, long date) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "channelNr=" + channelNr +
                ", unit='" + unit + '\'' +
                ", resolution=" + resolution +
                ", buffer=" + Arrays.toString(buffer) +
                '}' + "\n" +
                super.toString();
    }
}
