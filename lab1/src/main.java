
public class main
{
    public static void main(String[] args) {

        Integer buffer[] = {1,2,3,4,5,6,7,8,9,10};

        TimeHistory<Integer> timeHistory = new TimeHistory<Integer>(1, 2,"hz",10,buffer,"multimeter","My description", 120);
        Spectrum<Integer> spectrum = new Spectrum<Integer>();
        Alarm alarm = new Alarm(1,15,0,"Custom device", "Alarm description",10);

        System.out.println(timeHistory.toString() + "\n");
        System.out.println(spectrum.toString() + "\n");
        System.out.println(alarm.toString());
    }
}
