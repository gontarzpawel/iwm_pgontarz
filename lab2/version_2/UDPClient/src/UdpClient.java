import java.io.IOException;
import java.net.*;

public class UdpClient
{
    public static void main(String[] args) throws InterruptedException {

        DatagramSocket socket = null;
        String buff[] = {"buff1", "buff2", "buff3"};
        TimeHistory timeHistory = new TimeHistory(5, 5,"s",250,buff,"TimeHistory_device","TimeHistory_description",35);
        Spectrum spectrum = new Spectrum(10, 2,"hz",500,buff,"spectrum_device","Spectrum_description",150);
        Request requestForSpectrum = new Request(1,0,"spectrum_device", "Spectrum_description", 150);
        Request requestForTimeHistory = new Request(1,0,"TimeHistory_device", "TimeHistory_description", 35);
        Request badRequest = new Request(1,0,"BAD", "TimeHistory_description", 35);

        try
        {
            InetAddress aHost = InetAddress.getLocalHost();
            int serverPort = 9876;
            socket = new DatagramSocket();
            while (true)
            {
                Tools.sendData(spectrum, socket, aHost, serverPort, 500);
                Tools.sendData(timeHistory, socket, aHost, serverPort, 500);
                Spectrum spectrum1 = (Spectrum) Tools.sendRequest(requestForSpectrum, socket, aHost, serverPort,500);
                TimeHistory timeHistory1 = (TimeHistory) Tools.sendRequest(requestForTimeHistory, socket, aHost, serverPort,500);
                Tools.sendRequest(badRequest, socket, aHost, serverPort,500);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            socket.close();
        }
    }

}
