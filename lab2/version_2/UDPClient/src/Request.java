public class Request extends Packet
{
    public int type; // 0 -> transmit data, no response expected, 1 -> request for data from file device_description_date.txt
    public int value;

    public Request(int type, int value, String device, String description, long date)
    {
        super(device, description, date);
        this.type = type;
        this.value = value;
    }

    public Request()
    {
        super();
        this.type = 0;
        this.value= 0;
    }

    @Override
    public String toString() {
        return "Request{" +
                "type=" + type +
                ", value=" + value +
                '}'+
                super.toString();
    }

}
