import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

public class Tools
{
    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        }
        catch(IOException e) {
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Object deserialize(byte[] bytes) throws ClassNotFoundException {
        Object obj = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInputStream in = new ObjectInputStream(bis);
            obj = in.readObject();
            in.close();
            return obj;
        } catch (IOException e) {
            e.printStackTrace();
            return obj;
        }
    }

    public static Packet sendRequest(Request requestPacket, DatagramSocket socket, InetAddress aHost, int port, int timestamp)
    {
        try
        {
            System.out.println("----------------------------------------------------------------------------------------------");
            socket.setSoTimeout(2000);
            aHost = InetAddress.getLocalHost();
            byte[] data = Tools.serialize(requestPacket);
            DatagramPacket request = new DatagramPacket(data, data.length, aHost, port);
            //send request for download data
            socket.send(request);
            System.out.println("Send request for download");
            byte[] buffer = new byte[1024];
            DatagramPacket response = new DatagramPacket(buffer, buffer.length);
            socket.receive(response);
            System.out.println("Received data: \n" + Tools.deserialize(buffer) + "\n");
            TimeUnit.MILLISECONDS.sleep(timestamp);
            System.out.println("----------------------------------------------------------------------------------------------");
            if(Tools.deserialize(buffer).equals("[SERVER] Incorrect data request!"))
            {
                return null;
            }
            else
            {
                return (Packet)Tools.deserialize(buffer);
            }

        }
        catch (IOException | InterruptedException | ClassNotFoundException e)
        {
            System.out.println("[sendData] " + e.toString() + "\n");
            e.printStackTrace();
            return null;
        }


    }

    public static void sendData(Packet packet, DatagramSocket socket, InetAddress aHost, int port, int timestamp)
    {
        System.out.println("----------------------------------------------------------------------------------------------");
        Request requestPacket = new Request();
        try
        {
            aHost = InetAddress.getLocalHost();
            byte[] data = Tools.serialize(requestPacket);
            DatagramPacket request = new DatagramPacket(data, data.length, aHost, port);
            //send request for upload data
            socket.send(request);
            System.out.println("Send request for upload");
            TimeUnit.MILLISECONDS.sleep(10);
            data = Tools.serialize(packet);
            request = new DatagramPacket(data, data.length, aHost, port);
            //upload data
            socket.send(request);
            System.out.println("Send data: " + Tools.deserialize(request.getData()).toString() + "\n");
            //delay
            TimeUnit.MILLISECONDS.sleep(timestamp);
        }
        catch (IOException | InterruptedException | ClassNotFoundException e)
        {
            System.out.println("[sendData] " + e.toString() + "\n");
        }
        System.out.println("----------------------------------------------------------------------------------------------");
    }

}

