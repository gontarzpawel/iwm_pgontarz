import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SavePacket extends Thread
{
    public Packet packet;

    public SavePacket(Packet packet)
    {
        this.packet = packet;
    }

    @Override
    public synchronized void run() {
        try
        {
            String file_format = ".txt";
            String file_name = this.packet.getDevice() + "_" + this.packet.getDescription() + "_" + Long.toString(this.packet.getDate()) + file_format;
            OutputStream os = new FileOutputStream(file_name);
            // writes the bytes in a string object
            os.write(Tools.serialize(this.packet));
            os.close();
            //System.out.println("[SavePacket] Data from packet: \n" + packet.toString() + "\nhas been saved in file: " + file_name);
        }
        catch (IOException e)
        {
            System.out.println("[SavePacket] IOException: " + e.toString());
        }
    }
}

