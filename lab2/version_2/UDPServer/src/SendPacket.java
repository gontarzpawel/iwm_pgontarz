import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class SendPacket extends Thread
{
    private Request request;
    private DatagramSocket socket;
    private DatagramPacket reply;
    private byte[] content;

    public SendPacket(Request request, DatagramSocket socket, DatagramPacket reply)
    {
        content = null;
        this.socket = socket;
        this.request = request;
        this.reply = reply;
    }

    @Override
    public synchronized void run() {
        try
        {
            byte content[] = null;
            String str_content = "";
            String file_format = ".txt";
            String file_name = this.request.getDevice() + "_" + this.request.getDescription() + "_" + Long.toString(this.request.getDate()) + file_format;
            InputStream is = new FileInputStream(file_name);
            content = is.readAllBytes();
            is.close();
            System.out.println("[ReadPacket] Found requested data: \n" + Tools.deserialize(content).toString());
            //send msg
            DatagramPacket response = new DatagramPacket(content, content.length, reply.getAddress(), reply.getPort());
            socket.send(response);
            System.out.println("Send data to client: \n" + Tools.deserialize(content));
        }
        catch (IOException | ClassNotFoundException e)
        {
            System.out.println("[ReadPacket] IOException" + e.toString());
            String msg = "[SERVER] Incorrect data request!";
            DatagramPacket response = new DatagramPacket(Tools.serialize(msg), Tools.serialize(msg).length, reply.getAddress(), reply.getPort());
            try
            {
                socket.send(response);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            System.out.println("Send data to client\n");
        }
    }
}
