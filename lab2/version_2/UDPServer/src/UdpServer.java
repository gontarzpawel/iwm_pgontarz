import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpServer {
    public static void main(String[] args) throws ClassNotFoundException {
        DatagramSocket socket = null;

        try {
            InetAddress aHost = InetAddress.getLocalHost();
            socket = new DatagramSocket(9876, aHost);
            byte[] buffer = new byte[1024];
            while(true) {

                DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
                System.out.println("----------------------------------------------------------------------------------------------");
                System.out.println("Waiting for request...");
                socket.receive(reply);
                try
                {
                    Request read = (Request) Tools.deserialize(buffer);
                    //System.out.println(read.toString());
                    if(read.type == 0)
                    {
                        System.out.println("Receiving data: ");
                        socket.receive(reply);
                        Packet data = (Packet) Tools.deserialize(buffer);
                        SavePacket savePacket = new SavePacket(data);
                        savePacket.run();
                        System.out.println("Received: \n" + data.toString() +"\n\n");
                    }
                    else if(read.type == 1)
                    {
                        System.out.println("Request for data: " + "Device: " + read.device + ", Description: " + read.description + ", Date: " + Long.toString(read.date));
                        byte[] responseByteArray = null;
                        SendPacket sendPacket = new SendPacket(read, socket, reply);
                        sendPacket.run();
                    }
                    else
                    {
                        System.out.println("Bad request\n");
                    }

                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
            System.out.println("----------------------------------------------------------------------------------------------");
        }
    }
}