import java.io.IOException;
import java.net.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpClient
{
    public static void main(String[] args) throws InterruptedException {

        DatagramSocket socket = null;
        String buff[] = {"buff1", "buff2", "buff3"};
        Spectrum spectrum = new Spectrum(10, 2,"hz",500,buff,"Multimeter","My custom udp description",666);

        try {

            byte[] buffer = new byte[1024];
            InetAddress aHost = InetAddress.getLocalHost();
            int serverPort = 9876;
            socket = new DatagramSocket();

            byte[] data = Tools.serialize(spectrum);
            while (true) {
                DatagramPacket request = new DatagramPacket(data, data.length, aHost, serverPort);
                socket.send(request);
                System.out.println("Send datagram");
                DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
                socket.receive(reply); //get data from server

                try
                {
                    Packet read = (Packet) Tools.deserialize(buffer);
                    System.out.println(read.toString() + "\n\n");
                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }

                TimeUnit.MILLISECONDS.sleep(500);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}
