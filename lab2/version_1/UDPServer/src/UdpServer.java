import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpServer {
    public static void main(String[] args) {
        DatagramSocket socket = null;
        try {
            // args contain message content and server hostname
            InetAddress aHost = InetAddress.getLocalHost();
            socket = new DatagramSocket(9876, aHost);
            byte[] buffer = new byte[1024];
            while(true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                System.out.println("Waiting for request...");
                socket.receive(request);

                DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(), request.getAddress(), request.getPort());
                socket.send(reply);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}