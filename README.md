## Opis projektu zaliczeniowego - Aplikacja mobilna umożliwiająca stream danych pomiędzy mikrokontrolerem
* Aplikacja realizowana w ramach działalności w projekcie http://www.e-moto.agh.edu.pl/
* Aplikacja napisana w technologii Android przy użyciu języka Java [Klient]
    * wyszukiwanie urządzenia, które w swojej nazwie zawiera odpowiedni ciąg znaków
    * bindowanie z urządzeniem
    * po poprawnym nawiązaniu połączenia funkcjonalność odpowiedzialna za komunikację z gniazdem Bluetooth uruchamiana jest w globalnym wątku co umożliwia przechodzenie między aktywnościami nie tracąc połączenia
    * przesył parametrów odnośnie stanu baterii w motocyklu elektrycznym
    * przesył parametrów odnośnie danych silnika elektrycznego
    * przesył ogólnych parametrów jezdnych
    * przesył ogólnych parametrów motocykla tj. automatyczne światła czy klakson
    * pobieranie współrzędnych geograficznych na podstawie adresu (geocoding) i następnie przesył do mikrokontrolera w odpowiedniej formie
* Program na mikrokontroler Teensy 3.2 napisany został w języku C [Server]
    * obsługa potencjometrów na płytce uniwersalnej poprzez konwerter analogowo-cyfrowy (ADC)
    * obsługa przycisków na zasadzie przerwań
    * obsługa modułu Bluetooth HC-05
    * cykliczne wysyłanie, odbieranie, endkodowanie i dekodowanie parametrów transmisji 
---
Zdjęcia przedstawiające aplikację mobilną

<p align="center">
  <img src="images/img_1.png">
</p>

<p align="center">
  <img src="images/img_2.png">
</p>

<p align="center">
  <img src="images/img_3.png">
</p>

<p align="center">
  <img src="images/img_4.png">
</p>

<p align="center">
  <img src="images/img_5.png">
</p>

<p align="center">
  <img src="images/img_6.png">
</p>

<p align="center">
  <img src="images/img_7.png">
</p>

<p align="center">
  <img src="images/img_8.png">
</p>

<p align="center">
  <img src="images/img_9.png">
</p>

---
Zdjęcia przedstawiające mikrokontroler, moduł bluetooth oraz płytkę uniwersalną z potencjometrami oraz przyciskami

<p align="center">
  <img src="images/system.jpg">
</p>

<p align="center">
  <img src="images/urzadzenia.jpg">
</p>